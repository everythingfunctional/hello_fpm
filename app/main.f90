program hello_fpm
    use helloff, only: create_greeting

    implicit none

    print *, create_greeting("fpm")
end program hello_fpm
